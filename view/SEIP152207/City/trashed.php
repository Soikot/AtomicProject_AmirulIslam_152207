<?php
require_once("../../../vendor/autoload.php");
use App\BITM\SEIP152207\Message\Message;
if(!isset( $_SESSION)) session_start();

$message1=Message::message();



use App\BITM\SEIP152207\City\City;

$obj= new City();


$all_city= $obj->trashed();

?>
<!--table-->

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title> </title>

    <!-- Latest compiled and minified CSS -->
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="../../../resource/Bootstrap/css/bootstrap.min.css">

    <!-- Optional theme -->
    <link rel="stylesheet" href="../../../resource/Bootstrap/css/bootstrap-theme.min.css">
    <!-- <link rel="stylesheet" href="../../../resource/Bootstrap/css/booktitle.css"> -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
</head>

<style>
    .main{
        margin-top: 10%;
        margin-left: 15%;
        margin-right:15%;
        background-color: #679a9f;



    }
    body{
        background-image:url("../../../resource/assets/images/general2.jpg");

        background-repeat:no-repeat;
        background-size: 100% 925px;

    }



</style>

<body  >
<div class="container ">
    <div style="margin-top: 40px ;float: right;"> <a href="../index.php" class="btn btn-info btn-danger btn-lg" role="button">Atomic Project List</a></br></br></div>

    <div class="main">



        <div class="panel panel-default" >
            <div class="panel-heading">
                <div class="panel-heading">
                    <h1 style="text-align: center"> Trashed List</h1>


                </div>
            </div>





            <div class="panel-body">
                <form action="recovermultiple.php" method="post" id="multiple">
                    <div class="table-responsive" >
                        </br></br></br></br>
                        <table class="table">
                            <thead>
                            <tr>
                                <th>Select</th>
                                <th>#</th>
                                <th>ID</th>
                                <th>Name</th>
                                <th>City</th>

                                <th>Action</th>

                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <?php
                                $serial=0;

                                foreach($all_city as $city){
                                $serial++; ?>
                                <td><input type="checkbox" name="mark[]" value="<?php echo $city['id']?>"></td>
                                <td><?php echo $serial?></td>
                                <td><?php echo $city['id']?></td>
                                <td><?php echo $city['name']?></td>
                                <td><?php echo $city['city_name']?></td>
                                <td><a href="recover.php?id=<?php echo $city['id']  ?>" class="btn btn-primary" role="button">Recover</a>

                                    <a href="delete.php?id=<?php echo $city['id'] ?>" class="btn btn-danger" role="button" id="delete"  Onclick="return ConfirmDelete()">Delete</a>

                                </td>

                            </tr>
                            <?php }?>




                            </tbody>
                            <a href="index.php"  class="btn btn-info" role="button">Home</a> &nbsp;&nbsp;&nbsp;&nbsp;

                            <button type="submit" class="btn btn-info">Recover Selected</button>&nbsp;&nbsp;&nbsp;&nbsp;
                            <button type="button" class="btn btn-primary" id="delete">Delete all Selected</button>
                        </table>
                        <div id="confirmation_message" style="color:red;">
                            <?php echo $message1 ?>
                        </div>

                    </div>
                </form>

            </div>

        </div>

    </div>
</div>
<script>
    $(document).ready(function(){
        $(function() {
            $('#confirmation_message').delay(3000).fadeOut();

        });

    });
    $('#delete').on('click',function(){
        document.forms[0].action="deletemultiple.php";
        $('#multiple').submit();
    });

    function ConfirmDelete()
    {
        var x = confirm("Are you sure you want to delete?");
        if (x)
            return true;
        else
            return false;
    }
</script>



<script src="http://code.jquery.com/jquery-2.1.1.min.js"></script>
<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>

</body>
</html>



