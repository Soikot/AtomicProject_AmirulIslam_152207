<?php
require_once("../../../vendor/autoload.php");



//use App\BITM\SEIP128778\BookTitle;

$objBookTitle= new \App\BITM\SEIP152207\BookTitle\BookTitle();


$all_books= $objBookTitle->trashed();


?>
<!--table-->

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title> </title>

    <!-- Latest compiled and minified CSS -->
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="../../../resource/Bootstrap/css/bootstrap.min.css">

    <!-- Optional theme -->
    <link rel="stylesheet" href="../../../resource/Bootstrap/css/bootstrap-theme.min.css">
    <!-- <link rel="stylesheet" href="../../../resource/Bootstrap/css/booktitle.css"> -->
</head>

<style>
    .main{
        margin-top: 10%;
        margin-left: 15%;
        margin-right:15%;
        background-color: #679a9f;



    }
    body{
        background-image:url("../../../resource/assets/images/book.jpg");

        background-repeat:no-repeat;
        background-size: 100% 925px;

    }



</style>

<body  >
<div class="container ">

    <div class="main">



        <div class="panel panel-default" >
            <div class="panel-heading">
                <div class="panel-heading">
                    <h1 style="text-align: center"> BookTitle  Trashed List</h1>


                </div>
            </div>





            <div class="panel-body">
                <form action="recovermultiple.php" method="post" id="multiple">
                    <div class="table-responsive" >
                        </br></br></br></br>
                        <table class="table">
                            <thead>
                            <tr>
                                <th>Select</th>
                                <th>#</th>
                                <th>ID</th>
                                <th>Book title</th>
                                <th>Author</th>

                                <th>Action</th>

                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <?php
                                $serial=0;

                                foreach($all_books as $book){
                                $serial++; ?>
                                <td><input type="checkbox" name="mark[]" value="<?php echo $book['id']?>"></td>
                                <td><?php echo $serial?></td>
                                <td><?php echo $book['id']?></td>
                                <td><?php echo $book['book_title']?></td>
                                <td><?php echo $book['author_name']?></td>
                                <td>
                                    <a href="recover.php?id=<?php echo $book['id'] ?>"  class="btn btn-info" role="button">Recover</a> &nbsp;&nbsp;&nbsp;&nbsp;
                                    <a href="delete.php?id=<?php echo $book['id'] ?>" class="btn btn-danger" role="button" id="delete"  Onclick="return ConfirmDelete()">Delete</a>&nbsp;&nbsp;&nbsp;

                                </td>

                            </tr>
                            <?php }?>


                            </tbody>

                            <a href="index.php"  class="btn btn-info" role="button">Home</a> &nbsp;&nbsp;&nbsp;&nbsp;

                            <button type="submit" class="btn btn-primary">Recover Selected</button>&nbsp;&nbsp;&nbsp;&nbsp;
                            <button type="button" class="btn btn-info" id="delete">Delete all Selected</button>
                        </table>

                    </div>
                    <form>
            </div>

        </div>

    </div>
</div>
<script>
    $(document).ready(function(){
        $(function() {
            $('#confirmation_message').delay(3000).fadeOut();

        });

    });
    $('#delete').on('click',function(){
        document.forms[0].action="deletemultiple.php";
        $('#multiple').submit();
    });

    function ConfirmDelete()
    {
        var x = confirm("Are you sure you want to delete?");
        if (x)
            return true;
        else
            return false;
    }
</script>



<script src="http://code.jquery.com/jquery-2.1.1.min.js"></script>
<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>

</body>
</html>



